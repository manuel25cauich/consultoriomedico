<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="content-language" content="es">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('Consultorio', 'Consultorio') }}</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <style>
        /* Tamaño del scroll */
        .sidebar-sticky::-webkit-scrollbar {
            width: 8px !important;
        }

        /* Estilos barra (thumb) de scroll */
        .sidebar-sticky::-webkit-scrollbar-thumb {
            background: #ccc;
            border-radius: 4px;
        }

        .sidebar-sticky::-webkit-scrollbar-thumb:active {
            background-color: #999999;
        }

        .sidebar-sticky::-webkit-scrollbar-thumb:hover {
            background: #b3b3b3;
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
        }

        /* Estilos track de scroll */
        .sidebar-sticky::-webkit-scrollbar-track {
            background: #e1e1e1;
            border-radius: 4px;
        }

        .container::-webkit-scrollbar-track:hover,
        .container::-webkit-scrollbar-track:active {
            background: #d4d4d4;
        }

    </style>

</head>

<body class="">
    <div id="app">
        <div class="container-fluid">
            <main class="main">
                @yield('content')
            </main>
        </div>

    </div>
</body>
<footer>
    <script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
    <script src="{{ mix('js/app.js') }}"></script>

</footer>

</html>
