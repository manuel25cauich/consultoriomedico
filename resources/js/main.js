$('.dropdown-toggle').dropdown();
      
$("li").click(function () {

    if ($("#" + $(this).attr("id")).hasClass('show')) {
        $("#" + $(this).attr("id")).removeClass("show");
    } else {
        $("#" + $(this).attr("id")).addClass("show");
    }

});

$('#sne-toggle-sidebar').click(function () {
    sinoepShowHideSidebar();
});

$('#sne-back-sidebar').click( function () {

    if ( $('#sne-back-sidebar').attr('sne-sidebar-status') == 'show' ) {
        sinoepShowHideSidebar();
    }
    
});

function sinoepShowHideSidebar(params) {
    if ($('#sne-back-sidebar').is(':visible')) {

        $('#sne-sidebar').toggleClass('sidebar-left');
        $('#sne-back-sidebar').toggleClass('sne-siderbar-back-show');

        $('#sne-toggle-sidebar').toggleClass('open');

        $('#sne-back-sidebar').attr('sne-sidebar-status', 'hide');
        
        setTimeout(function(){
            $('#sne-sidebar').toggleClass('sne-sidebar-hide');
            $('#sne-back-sidebar').toggleClass('sne-sidebarback-hide');
        }, 200); 

    } else {

        $('#sne-sidebar').toggleClass('sne-sidebar-hide');
        $('#sne-back-sidebar').toggleClass('sne-sidebarback-hide');

        $('#sne-toggle-sidebar').toggleClass('open');

        $('#sne-back-sidebar').attr('sne-sidebar-status', 'show');

        setTimeout(function(){
            $('#sne-sidebar').toggleClass('sidebar-left');
            $('#sne-back-sidebar').toggleClass('sne-siderbar-back-show');
        }, 20);
        
    }
}



/****************************************************/
/**                      TEST                      **/
/****************************************************/
$('.circle-plus').on('click', function(){
    $(this).toggleClass('opened');
  })