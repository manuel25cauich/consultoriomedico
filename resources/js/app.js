require('./bootstrap');


// imports pluginn sistema 
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vue2Editor from "vue2-editor";
import Router from './Routes';
import es from 'vuetify/es5/locale/es';
// import 
import App from "./components/App.vue";
import VuetifyDialog from 'vuetify-dialog'
import 'vuetify-dialog/dist/vuetify-dialog.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(VuetifyDialog, {
  context: {
    Vuetify
  }
})

window.Swal = require("sweetalert2");

Vue.use(Vue2Editor);


//declaracion componentes
Vue.prototype.$spinner = function () {
    Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        allowEnterKey: false,
        timerProgressBar: true,
        background: '#ffffff00',
        onBeforeOpen: () => {
            Swal.showLoading()
        }
    });
};

Vue.use(Vuetify,{
    iconfont: 'mdi'
})



new Vue({
    vuetify: new Vuetify({lang:{ locales: {es}, current: 'es'}}),
    el: '#app',
    router:Router,
    render:h=>h(App)
    // components:{App}
});
