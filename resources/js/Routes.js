import Vue from 'vue';
import VueRouter from 'vue-router';

import Doctores from "./components/doctores/doctores.vue";
import Consultorio from "./components/consultorios/consultorio.vue";
Vue.use(VueRouter);



const routes = [
    {
        path: "/", component: Doctores, name: "home",
        

    }, 
    {path: "/consultorios/:doctor", component: Consultorio, name: "consultorio", props: true}
    
];

const router = new VueRouter({
    mode: "history",
    routes:routes
});

export default router;
