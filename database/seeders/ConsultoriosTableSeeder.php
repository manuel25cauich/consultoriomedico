<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsultoriosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('consultorios')->insert([
            "id" => 2,
            "doctor_id" => 1,
            "descripcion" => "Consultorio 1",
            "created_at" => "2022-03-05 15:53:22",
            "updated_at" => "2022-03-05 16:14:36",
          ]);
          
          DB::table('consultorios')->insert([
            "id" => 3,
            "doctor_id" => 3,
            "descripcion" => "Consultorio2",
            "created_at" => "2022-03-05 16:15:05",
            "updated_at" => "2022-03-05 16:15:05",
          ]);
          
    }
}
