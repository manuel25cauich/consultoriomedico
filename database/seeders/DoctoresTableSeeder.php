<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DoctoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doctores')->insert([
            "id" => 1,
            "nombre" => "Manuel",
            "apellido_m" => "Caballero",
            "apellido_p" => "Cauich",
            "direccion_id" => 1,
            "created_at" =>  NULL,
            "updated_at" => "2022-03-05 14:35:21",
          ]);
          
          DB::table('doctores')->insert([
            "id" => 3,
            "nombre" => "laura",
            "apellido_m" => "oliva",
            "apellido_p" => "chan",
            "direccion_id" => 6,
            "created_at" => "2022-03-05 06:28:46",
            "updated_at" => "2022-03-05 06:28:46",
          ]);
    }
}
