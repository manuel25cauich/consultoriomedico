<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DireccionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('direccion')->insert([
            "id" => 1,
            "calle" => "conocidos",
            "cruzamiento" => "conocidos",
            "codigo_postal" => "77086",
            "cruzamiento2" => "conocido",
            "created_at" =>  NULL,
            "updated_at" => "2022-03-05 15:13:28",
          ]);
          
          DB::table('direccion')->insert([
            "id" => 6,
            "calle" => "as",
            "cruzamiento" => "saf",
            "codigo_postal" => "12345",
            "cruzamiento2" => "fdv",
            "created_at" => "2022-03-05 06:28:46",
            "updated_at" => "2022-03-05 06:28:46",
          ]);
    }
}
