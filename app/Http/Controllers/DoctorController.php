<?php

namespace App\Http\Controllers;

use App\Models\ConsultorioModel;
use App\Models\DireccionModel;
use App\Models\DoctoresModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctores = DoctoresModel::with("direccion")->get();
        return array("data" => $doctores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            DB::beginTransaction();
            $direccion = new DireccionModel();
            $direccion->calle = $request->calle;
            $direccion->cruzamiento = $request->cruzamiento;
            $direccion->cruzamiento2 = $request->cruzamiento2;
            $direccion->codigo_postal = $request->codigo_postal;
            $direccion->save();
            if (is_null($direccion)) {
                $doctor = new DoctoresModel();
                $doctor->nombre = $request->nombre;
                $doctor->apellido_p = $request->apellido_p;
                $doctor->apellido_m = $request->apellido_m;
                $doctor->direccion_id = $direccion->id;
                $doctor->save();
            } else {
                throw new Exception('Error', 1);
            }


            DB::commit();
            return array("status" => true, "msg" => "Creado correctamente");
        } catch (Exception $e) {
            DB::rollBack();
            return array("msg" => "Ocurrio un erro al registrar al doctro", "status" => false);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $direccion = new DireccionModel();
            $direccion->calle = $request->calle;
            $direccion->cruzamiento = $request->cruza;
            $direccion->cruzamiento2 = $request->cruza2;
            $direccion->codigo_postal = $request->cp;
            $direccion->save();
            if (!is_null($direccion)) {
                $doctor = new DoctoresModel();
                $doctor->nombre = $request->nombre;
                $doctor->apellido_p = $request->apellidoP;
                $doctor->apellido_m = $request->apellidoM;
                $doctor->direccion_id = $direccion->id;
                $doctor->save();
            } else {
                throw new Exception('Error', 1);
            }


            DB::commit();
            return array("status" => true, "msg" => "Creado correctamente");
        } catch (Exception $e) {
            DB::rollBack();
            return array("msg" => $e->getMessage(), "status" => false);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try {
           DB::beginTransaction();
           $direccion =  DireccionModel::find($request->direccion_id);
           $direccion->calle = $request->calle;
           $direccion->cruzamiento = $request->cruza;
           $direccion->cruzamiento2 = $request->cruza2;
           $direccion->codigo_postal = $request->cp;
           $direccion->save();

           $doctor =  DoctoresModel::find($request->id);
           $doctor->nombre = $request->nombre;
           $doctor->apellido_p = $request->apellidoP;
           $doctor->apellido_m = $request->apellidoM;
           $doctor->direccion_id = $direccion->id;
           $doctor->save();

           DB::commit();
            return array("status"=>true,"msg"=>"Actualizado con éxito");
       } catch (Exception $e) {
           DB::rollBack();
           return array("status"=>false, "msg"=>"¡Ocurrio un error mientras se actualizaba!, intente nuevamente");
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $doctor = DoctoresModel::where("id",$id)->first();
            if($doctor){
                ConsultorioModel::where("doctor_id",$id)->delete();
                $doctor->delete();
                DireccionModel::where('id',$doctor->direccion_id)->delete();
            }
            DB::commit();
            return array("msg"=>"Eliminado con éxito", "status"=>true);
        } catch (Exception $e) {
            //throw $th;
            DB::rollBack();
            // Algo salio mal al realizar la operación intente nuevamente
            return array("msg"=>$e->getMessage(), "linea"=>$e->getLine(), "status"=>false);

        }
    }
}
