<?php

namespace App\Http\Controllers;

use App\Models\ConsultorioModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConsultorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $consultorio = new ConsultorioModel();
            $consultorio->doctor_id = $request->doctor_id;
            $consultorio->descripcion = $request->nombre;
            $consultorio->save();
            DB::commit();
            return array("status" => true, "msg" => "Se agrego con éxito");
        } catch (Exception $e) {
            DB::rollback();
            return array("status" => false, "msg" => $e->getMessage());
            // "¡hubo un error al registrar los datos!, intente nuevamente");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $consultorios = ConsultorioModel::where("doctor_id", $id)->get();
        foreach ($consultorios as $value) {
            $nombre_doctor = $value->doctor_nombre_completo($value->doctor_id);
            $value->doctor = $nombre_doctor; 
        }
        return array("data" => $consultorios);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $consultorio = ConsultorioModel::where("id",$id)->first();
            $consultorio->descripcion = $request->nombre;
            $consultorio->save();
            DB::commit();
            return array("status" => true, "msg" => "¡Se actualizo con éxito!");
        } catch (Exception $e) {
            DB::rollBack();
            return array("status" => false, "msg" =>$e->getMessage());
            // "¡hubo un error al modificar los datos!, intente nuevamente");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            ConsultorioModel::where("id", $id)->delete();
            DB::commit();
            return array("status" => true, "msg" => "Se elimino con éxito");
        } catch (Exception $e) {
            DB::rollBack();
            return array("status" => false, "msg" => "¡hubo un error al eliminar los datos!, intente nuevamente");
        }
    }
}
