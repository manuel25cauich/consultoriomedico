<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctoresModel extends Model
{
    protected $table = 'doctores';
    protected $guarded  = [];

    public function direccion()
    {
        return $this->belongsTo(DireccionModel::class);
    }
}
