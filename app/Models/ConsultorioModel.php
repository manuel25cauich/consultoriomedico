<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultorioModel extends Model
{
    protected $table = 'consultorios';
    protected $guarded  = [];

    public static function doctor_nombre_completo($doctor_id)
    {   
        $doctor = DoctoresModel::find($doctor_id);
       return $doctor->nombre." ".$doctor->apellido_p." ".$doctor->apellido_m;
    }
}
