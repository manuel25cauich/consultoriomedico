<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DireccionModel extends Model
{
    protected $table = 'direccion';
    protected $guarded  = [];
}
